**Web Scrapper**


## Description

The script allows you to create a text document with a list of all links
on a site; from a URL the user has supplied through an input field and
the option to download and save any PDFs(or any filetype, by changing the
extension in the script) in a folder named as the provided URL; for easy 
identification.

Download progress bars indicate the progress for each downloading file.


## Dependencies

The script depends on the following Python version and modules to function.


* Python 3xx
* os
* requests  [Install requests](https://pypi.org/project/requests/)
* urllib  [Install urllib](https://pypi.org/project/urllib3/)
* bs4  [Install beautifulsoup4](https://pypi.org/project/beautifulsoup4/)
* progress  [Install progress](https://pypi.org/project/progress/)


## Usage

Make sure to change all paths in the script to match yours first.

```bash

/home/awphel/Albatross/output/

/home/awphel/Albatross/downloads/
```

Run this command in your command line terminal :

```python

python wscraper.py

or

python3 wscraper.py
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
