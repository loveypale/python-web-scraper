"""
author  Lovey Pale
version 1.0.0, 7/03/2019

"""
import os
import requests
import urllib.request
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from progress.bar import ChargingBar

#Feed Python the web adress
url = str(input('Enter url : '))
print('')#BLANK LINE
print('')#BLANK LINE
response = requests.get(url)

#parse URL to BeautifulSoup as text
soup = BeautifulSoup(response.text, 'html.parser')

#Create variables to be used for the title of the document
#for our records and filename
header = soup.find('title')
site_name = str(header.renderContents())
doc_name = site_name.replace("\\", "")
#The path and filename of the document to be saved has to corrolate with
#being scrapped for the records to make any sense.
extension = '.txt'
save_path = '/home/awphel/Albatross/output/' + doc_name + extension

#Extract all URLs from the website
for link in soup.find_all('a'):
    data = str(link.get('href'))

#Creating the document and popullating it with our data
    file = open('/home/awphel/Albatross/output/'+ doc_name + extension,'a')
    file.write(data)
    file.write('\n')
    file.close()

print('... The link data is ready in ' '\033[1m'+ doc_name + extension + '\033[0m')
print('')#BLANK LINE
print('')#BLANK LINE
print('')#BLANK LINE

#Now that we've got the links we wanted, we can now proceed
# to download any data files available for download  as specified below
filetype = input('\033[1m'+'Press ENTER to download files from site' + '\033[0m')
print('')#BLANK LINE

#If there is no such folder, the script will create one automatically
folder_location = "/home/awphel/Albatross/downloads/" + doc_name
if not os.path.exists(folder_location):os.mkdir(folder_location)

for link in soup.select("a[href$='.pdf']"):

    #Name the pdf files 
    filename = os.path.join(folder_location,link['href'].split('/')[-1])

    #Display files being downloaded
    with open(filename, 'wb') as f:

        # Download the files
        f.write(requests.get(urljoin(url,link['href'])).content)

        #Add a progress bar indicator for the downloads
        bar = ChargingBar('downloading', index=99)
        print('Downloading ' + filename)
        bar.next()
        bar.finish()

print('')#BLANK LINE

#Done Downloading and saving files, inform user
print('\033[1m' + '... Complete' + '\033[0m')
print('')#BLANK LINE
print('Your files are ready in the ' + '\033[1m' + 'downnloads ' + '\033[0m' + 'folder')
print('')#BLANK LINE
